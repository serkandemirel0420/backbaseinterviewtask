# General #
Backbase Frontend Developer Exercise 2.4.

### Explanation ###
* Simpe weather application that uses OpenWheater dummy API.
* [OpenWeather API](http://openweathermap.org/api)

### Plugins and Frameworks ##
* [Angular 1.5.11](https://code.angularjs.org/1.5.11/docs/guide/component)
* [angular-chart](http://jtblin.github.io/angular-chart.js/)
* [Semantic UI](http://semantic-ui.com)

### Running ###
* Clone the repo then open the Index.html file.

### Owner ###
* Serkan Demirel
* Email : serkandemirel0420@gmail.com