var interviewTask = angular.module('interviewTask',['chart.js'])
interviewTask.constant('apiEndPoint', 'http://api.openweathermap.org/data/2.5');


//main list component
interviewTask.component('app', {
    template: '<ng-outlet> </ng-outlet>'
})


//city list component
interviewTask.component('cities',{
    template: '<div class="ui container"> <img class="mySpinner" src="http://www.mvgen.com/loader.gif" ng-show="loading" /> <div class="city" ng-repeat="city in cities track by $index"> <div class="ui cards"> <div class="card"> <div class="content"> <a ng-click="seaLevel(city)"> <i class="wi wi-owm-night-{{city.weather[0].id}}"></i> <span class="tempSpan">{{city.main.temp }} °C</span> </a> <div class="header"> <a ng-click="seaLevel(city)"> <h1>{{city.name}}</h1> </a> </div> <div class="meta"> ( {{ city.weather[0].description | lowercase }} ) </div> <div class="ui stackable four column grid"> <div class="column columnPressure description">Pressure : {{city.main.pressure }}</div> <div class="column columnHumidity description">Humidity : {{city.main.humidity }}</div> <div class="column columnSunrise description">Sunrise : {{city.sys.sunrise * 1000 | date:\'h:mma\' }}</div> <div class="column columnSunset description">Sunset : {{city.sys.sunset * 1000 | date:\'h:mma\'}}</div> </div> </div> </div> </div> <div class="seaLevel" ng-show="selectedCity && city.id == selectedCity.id"> <h3>{{selectedCity.name}} Sea Level - Time </h3> <canvas id="line" class="chart chart-line" chart-data="data"chart-labels="labels" chart-series="series" chart-options="options"chart-dataset-override="datasetOverride" chart-click="onClick"> </canvas </div> </div> </div>',
    controller: 'WeatherController'
});



interviewTask.controller('WeatherController', function ($scope, weatherService, fiveDaysForecast) {
    //show loading gif
    $scope.loading = true;

    //service call for weather data
    var promise = weatherService.getWeather();
    promise.then(function(result){
        $scope.cities = result.data.list;

        //remove loading gif
        $scope.loading = false;
    });

    $scope.seaLevel = function(city) {

        cityId = city.id;

        //service call for forecast data
        var promise = fiveDaysForecast.getFiveDaysForecast(cityId)

        promise.then(function(result){
          
            //filter data for 09:00AM value and map to named arrays for chart.
            var filteredData = result.data.list.filter(function(field){
                return field.dt_txt.indexOf('09:00:00') > -1
            }).reduce(function(acc, o) {
                acc.sea_levels.push(o.main.sea_level);
                acc.dates.push(o.dt_txt);
                return acc;
            }, {sea_levels: [], dates: []});
   
         
          
            //selected city
            $scope.selectedCity = result.data.city;
             

            //chart sea level dates array
            $scope.labels = filteredData.dates;
            
            //chart sea level data array
            $scope.data = [ filteredData.sea_levels ];

            //chart settings
            $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
            $scope.options = {
                scales: {
                    yAxes: [
                            {
                            id: 'y-axis-1',
                            type: 'linear',
                            display: true,
                            position: 'left'
                            }],
                            xAxes: [{
                                display: false
                            }]
                }
            };

        });
    };


    

});

 

//weather Service
interviewTask.service("weatherService", function($http, $q,apiEndPoint){
    this.getWeather = function(){
        return  $http.get(apiEndPoint + '/group?id=2950159,524901,745044,2643743,2759794&units=metric&APPID=3d8b309701a13f65b660fa2c64cdc517');
    }

})


//forecast Service
interviewTask.service("fiveDaysForecast", function($http, $q,apiEndPoint){   
    this.getFiveDaysForecast = function(id){
        return $http.get(apiEndPoint + '/forecast?id='+id+'&appid=3d8b309701a13f65b660fa2c64cdc517'); 
    }

})